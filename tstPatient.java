package code;

import org.junit.Assert;



public class tstPatient {
	
@org.junit.Test   
public void ConstructorTest() {
	Patient p= new Patient("Eid","Saeed");
	Assert.assertTrue(p.getfname().equals("Eid"));
	Assert.assertTrue(p.getlname().equals("Saeed"));
	
	p.setSSN("123abc456");
	Assert.assertTrue(p.getSSN().equals("123abc456"));

	p.setPhNum("010011001100");
	Assert.assertTrue(p.getPhNum().equals("010011001100"));
}
}

